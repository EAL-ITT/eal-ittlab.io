author:            Anatoli Penev
summary:           How to use claat-tools
id:                using-claat
categories:        static page
environments:      markdown
status:            final
feedback link:     https://gitlab.com/19s_itt4_internship/claat-template-system/issues
analytics account: 0

# How to use claat tool

## Overview of the tutorial

Duration: 1:00

This tutorial shows you how to use claat tools and make an example page. In this tutorial you will do the following:

* Learn how to write a .md format accepted by claat.
* Push the template to your gitlab page and run it automatically

## Markdown template

Duration: 30:00

Claat uses simple markdown syntax and converts them to html static pages. Only requirment is that the document is made in segments (or steps) and *Duration* is added to each step for better understanding how long it takes.

A requirment is to have a summary on top of the .md document which will be used as header and some links. The template syntax markdown, but **claat** takes specific templating. You can copy/paste the code and change to what suits you. You can add as many steps/slides as you need, as long as they follow this guide. (see code bellow)

```markdown
author:            Author name
summary:           What is the document for
id:                Folder name/id
categories:        What is being created (usually static page)
environments:      Document code?(usually markdown)
status:            Document version (alpha, beta, final)
feedback link:     Input for contact link
analytics account: Always 0

# Name of template/guide

## Name of first slide/step

Duration: Timeframe of how much it will take to go over the template

Text of the slide/step
(follows markdown syntax)

## Name of second slide/step

Duration: Timeframe for second slide(each slide has its own duration)

You can use links also (again following the markdown syntax)
[Name for link](full url here).

Pictures can be also embedded.
![tooltip for picture](path for picture)

Videos can be added but only as links to source using same syntax as url.
```

Here is a complete guide on the claat formatting.(Will be downloaded to your PC.) [Codelab Formatting Guide](https://gitlab.com/19s_itt4_internship/claat-template-system/raw/master/Codelab_Formatting_Guide.pdf?inline=false)

## Simple frontpage

In order for the template to be seen, here is a simple frontpage. You can change the logo and names as you please and link to your template. The HTML file along with the IMG (if any) are placed in the **public** folder in the project repository. The ID of guide in the link is taken when you create the .md file from the header metadata.

```html
<!DOCTYPE html>
<html>
    <head>
            <title>Name of your page</title>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    </head>
<body>
        <img src="name_of_image" alt="mouseovertext(usually same as logo)">
<div>
<ul>
    <ul style="list-style-type:disc;">
        <li><a href="https://name_of_your_repository/project_name/id_of_guide/index.html">Name_of_the_template</a></li>
    </ul>
</ul>
</div>

</body>
</html>
```

## Running claat offline (usually for testing purposes)

Duration: 10:00

1. Have the .md file and all pictures (if any are being used) ready in a folder you chose.
2. Make sure this directory path is correct inside the .md file.
3. Before you run the claat command, you need to give executable permission to
the binary **chmod +x claat-name_of_file_you_have** (example *chmod +x claat-linux-amd64*)
4. When running the command you have to be in same folder as the binary file is. You can also export it to your PATH so you can run it from anywhere.
5. Run the command ```./claat-name_of_file_you_have export filename```
We use **--prefix=../** so we can have all in one folder, regardles of claat default output.
6. Run the command ```./claat-name_of_file_you_have serve filename```. This will fetch and create the elements and components needed for the styling and will run local server and open your file. Make sure before you run this command, to have the .md file in the **same** directory as you generated in Step 3.
7. Now you have your guide up and running locally. If the output is as expected, you can publish it in a Gitlab (**next step**) and create a page and share it.

*It is worth to add the directory that contains the claat binary to PATH to have this tool available from any place. In my case in ~/.bashrc I have added that line:* <br>
**export PATH=$PATH:/home/username/bin/**

## Publishing the guide with Gitlab

Duration: 15:00

First you need to enable CI/CD in your repository for auto-deployment. This is the default preset.
![AutoDevs](images/using-claat/auto_devs.png)

In order to publsih a Gitlab page you need a YAML file. This is an example CI file that Gitlab runner uses for pipelines and AutoDev jobs. More detailed guide can be found [YAML_documentation](https://docs.gitlab.com/ee/ci/yaml/).

```yaml
pages:
  stage: deploy
  before_script:
  - wget https://github.com/googlecodelabs/tools/releases/download/v2.2.5/claat-linux-amd64 - (we keep this here as it will download claat and run for you automatically)
  - chmod +x claat-linux-amd64 (here we give permission for the tool to run automatically)
  script:
  - ./claat-linux-amd64 export template/claat-guide.md (this command will execute the script and the .md file that will be converted. the value needs to be changed to the location of your file and the name of the file.)
  - mkdir -p public (we make public folder to store our template and for the page to be published)
  - cp claat-guide public -r
  artifacts:
    paths:
    - public (with artifacts, we are saving our published guide so Gitlab can access and run it.)
```

Here is a video about Minimal CI with Gitlab: [Gitlab Minimal CI](https://www.youtube.com/watch?v=9Y5sX1Y1PiY)<br>
Creating GitLab pages: [GitLab pages](https://19s_itt4_internship.gitlab.io/tech-guides/gitlab-pages/index.html#0)

After this is done, your repository will run each **push** you do via pipelines.
Progress can be checked in the Pipelines tab.

<img src="images/using-claat/pipelines.png" width="200">