author:            Nikolaj Simonsen
summary:           Gitlab daily workflow
id:                gitlab_daily_workflow
categories:        static page
environments:      git, gitlab
status:            beta
feedback link:     https://gitlab.com/npes/claat-generator/issues
analytics account: 0

# Gitlab daily workflow

## Introduction

The purpose of this document is to give lecturers and students an overview of the daily workflow using git and Gitlab.

### Description

This document covers only the very basic use of git and Gitlab regarding the version control features and backup features. 

### Gitlab overview

[Gitlab](https://about.gitlab.com/what-is-gitlab/) is an application for the DevOps lifecycle, it includes continuous integration, git version control and more.

Gitlab is not the same as git. git is a technology invented by Linus Torvalds, Gitlab is using git as their version control technology but offers much more on top of git.

### Repositories

A repository is a git term, in layman terms it is a location which can store files and keep track of changes using version control.

A repository can be located in the cloud (remote) or locally. 

Usually you will do your daily work in a local copy of the remote repository.

More than one person can work with files in the same repository, this means that you can experience conflicts if two persons have been editing the same file.

To practice your gitlab skills this [test repository](https://gitlab.com/npes/test_repository) can be used throughout this codelab.

You can also setup a repository in gitlab by clicking the [New project](https://gitlab.com/projects/new) button when logged in to gitlab.  
Fill in the nessesary information and click the checkbox for `Initialize repository with a README` to be able to immediately clone the repository.

![Figure 0](images/git_daily_workflow_guide/gitlab_new_project.PNG "Gitlab new project")

### .git

A hidden folder named `.git` is located in a local git repository folder, this file contains all the information that is necessary for your project in version control and all the information about commits, remote repository address etc. All are present in this folder. It also contains a log which stores your commit history so that you can roll back to history.

This means that the local repository folder can be moved to any location on your machine, just make sure the .git folder is kept. 

### .gitignore

A Hidden file called .gitignore can be created inside the root of the git repository. In this file you can specify which files not to include in the remote repository. See [the official git documentation](https://git-scm.com/docs/gitignore)

## Pre-requisites

The steps in the pre-requisites is only needed one time per computer.

### Git

To work with git you need to install git from [git-scm](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

### Gitlab account

To work with gitlab you need to have a Gitlab account. Register your account on [Gitlab](https://gitlab.com/users/sign_in)

### SSH keys

To secure the transfers between Gitlab repositories and local repositories you need to setup SSH keys. Follow this [guide](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair)


## Daily workflow



### Cloning a repository to your machine

To get a copy of the remote repository to your local machine do the following

1. go to your repository or use the [test repository](https://gitlab.com/npes/test_repository) for practice
2. click the `Clone` button
3. copy the `Clone with SSH` link  
![Figure 1](images/git_daily_workflow_guide/clone_with_ssh.PNG "Clone with SSH") 
4. open `git bash` or another terminal that supports git bash (The screenshot shows how to open git bash from the windows context menu)  
![Figure 2](images/git_daily_workflow_guide/git_bash_context.PNG "Git bash context menu")
5. navigate to the folder where you want the repository cloned to
6. type `git clone <SSH link>` example: `git clone git@gitlab.com:npes/test_repository.git`  
![Figure 3](images/git_daily_workflow_guide/git_clone.PNG "Git bash clone")  
If successful you now have a clone of the git repository on your machine

### Update local repository (pull)

More users can change files in the remote repository and it is adviced to update your local repository before pushing your own changes to the remote repository.  
This is to ensure that possible conflicts is resolved in your local repository and not pushed to the remote repository.

1. Open a git bash terminal from the local repository folder or navigate to it from the terminal
2. type `git pull` to pull changes from the remote repository to your local repository  
![Figure 4](images/git_daily_workflow_guide/git_pull.PNG "Git bash pull")

### Add changes to local repository (git add, git commit)

Commiting changes to your local repository is done in two steps, first you need to add your changed files (staging), then you need to commit the staged files.

1. open a git bash terminal from the local repository folder or navigate to it from the terminal
2. type `git add <filename>` to stage one file or git `add .` to stage all files.
3. type `git commit -m "commit message"` replace the text `commit message` with a short description of the changes
![Figure 5](images/git_daily_workflow_guide/git_add_commit.PNG "Git bash commit")

Your changes is now added and committed to your local repository but not to the remote repository.

### Update remote repository (push)

To update the remote repository with your committed changes you have to push to the remote repository.

1. open a git bash terminal from the local repository folder or navigate to it from the terminal
2. type `git push`  
![Figure 6](images/git_daily_workflow_guide/git_push.PNG "Git bash push")

Your changes is now pushed to the remote repository

## Further reading

The official [git documentation](https://www.git-scm.com/doc) which also includes videos.  
The official [Gitlab documentation](https://docs.gitlab.com/ee/README.html)  
If something goes wrong [ohshitgit](http://ohshitgit.com/)  
git [cheat sheet from git tower](https://www.git-tower.com/blog/git-cheat-sheet)  
git [cheat sheet from Gitlab](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)  
a nicer looking alternative to git bash is [cmder](https://cmder.net/)  
if you get tired of writing your ssh passphrase when interacting with git try [ssh agent in cmder](https://github.com/cmderdev/cmder/wiki/Integrate-SSH-Agent-or-PuTTY-Agent)